package EShop.lab2

import akka.actor.{Actor, ActorRef, Cancellable, Props}
import akka.event.Logging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

object Checkout {

  sealed trait Data
  case object Uninitialized                               extends Data
  case class SelectingDeliveryStarted(timer: Cancellable) extends Data
  case class ProcessingPaymentStarted(timer: Cancellable) extends Data

  sealed trait Command
  case object StartCheckout                       extends Command
  case class SelectDeliveryMethod(method: String) extends Command
  case object CancelCheckout                      extends Command
  case object ExpireCheckout                      extends Command
  case class SelectPayment(payment: String)       extends Command
  case object ExpirePayment                       extends Command
  case object ReceivePayment                      extends Command

  sealed trait Event
  case object CheckOutClosed                   extends Event
  case class PaymentStarted(payment: ActorRef) extends Event

  def props(cart: ActorRef) = Props(new Checkout())
}

class Checkout extends Actor {

  import Checkout._

  private val scheduler = context.system.scheduler
  private val log       = Logging(context.system, this)

  val checkoutTimerDuration = 1 seconds
  val paymentTimerDuration  = 1 seconds

  @Override
  def receive: Receive = {
    case StartCheckout =>
      context become selectingDelivery(scheduler.scheduleOnce(checkoutTimerDuration, self, ExpireCheckout))
  }

  def selectingDelivery(timer: Cancellable): Receive = {
    case SelectDeliveryMethod(method) =>
      log.info("Selected delivery method: " + method)
      context become selectingPaymentMethod(timer)

    case CancelCheckout | ExpireCheckout =>
      context become cancelled
  }

  def selectingPaymentMethod(timer: Cancellable): Receive = {
    case CancelCheckout | ExpireCheckout =>
      context become cancelled

    case SelectPayment(payment) =>
      timer.cancel()
      log.info("Selected payment" + payment)
      context become processingPayment(scheduler.scheduleOnce(paymentTimerDuration, self, ExpirePayment))
  }

  def processingPayment(timer: Cancellable): Receive = {
    case CancelCheckout | ExpirePayment =>
      log.info("Payment expired")
      context become cancelled

    case ReceivePayment =>
      timer.cancel()
      log.info("Payment received")
      context become closed
  }

  def cancelled: Receive = {
    case msg: Any => log.error("Received msg in cancelled state: " + msg)
  }

  def closed: Receive = {
    case msg: Any => log.error("Received msg in closed state: " + msg)
  }
}
