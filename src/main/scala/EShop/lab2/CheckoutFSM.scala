package EShop.lab2

import EShop.lab2.Checkout.{
  CancelCheckout,
  Data,
  ExpireCheckout,
  ExpirePayment,
  ProcessingPaymentStarted,
  ReceivePayment,
  SelectDeliveryMethod,
  SelectPayment,
  SelectingDeliveryStarted,
  StartCheckout,
  Uninitialized
}
import EShop.lab2.CheckoutFSM.Status
import akka.actor.{ActorRef, LoggingFSM, Props}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

object CheckoutFSM {

  object Status extends Enumeration {
    type Status = Value
    val NotStarted, SelectingDelivery, SelectingPaymentMethod, Cancelled, ProcessingPayment, Closed = Value
  }

  def props(cartActor: ActorRef) = Props(new CheckoutFSM)
}

class CheckoutFSM extends LoggingFSM[Status.Value, Data] {
  import EShop.lab2.CheckoutFSM.Status._

  // useful for debugging, see: https://doc.akka.io/docs/akka/current/fsm.html#rolling-event-log
  override def logDepth = 12

  val checkoutTimerDuration: FiniteDuration = 1 seconds
  val paymentTimerDuration: FiniteDuration  = 1 seconds

  private val scheduler = context.system.scheduler

  startWith(NotStarted, Uninitialized)

  when(NotStarted) {
    case Event(StartCheckout, Uninitialized) =>
      goto(SelectingDelivery).using(
        SelectingDeliveryStarted(scheduler.scheduleOnce(checkoutTimerDuration, self, ExpireCheckout))
      )
  }

  when(SelectingDelivery) {
    case Event(SelectDeliveryMethod(method), SelectingDeliveryStarted(timer)) =>
      goto(SelectingPaymentMethod).using(Uninitialized)

    case Event(CancelCheckout, _) | Event(ExpireCheckout, _) =>
      goto(Cancelled).using(Uninitialized)
  }

  when(SelectingPaymentMethod) {
    case Event(CancelCheckout, _) | Event(ExpireCheckout, _) =>
      goto(Cancelled).using(Uninitialized)

    case Event(SelectPayment(payment), Uninitialized) =>
      goto(ProcessingPayment).using(
        ProcessingPaymentStarted(scheduler.scheduleOnce(paymentTimerDuration, self, ExpirePayment))
      )
  }

  when(ProcessingPayment, stateTimeout = paymentTimerDuration) {
    case Event(CancelCheckout, _) | Event(ExpirePayment, _) =>
      goto(Cancelled).using(Uninitialized)

    case Event(ReceivePayment, _) =>
      goto(Closed).using(Uninitialized)
  }

  when(Cancelled) {
    case Event(_, _) => stay()
  }

  when(Closed) {
    case Event(_, _) => stay()
  }

}
