package EShop.lab2

import akka.actor.{Actor, ActorRef, Cancellable, Props}
import akka.event.{Logging, LoggingReceive}

import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.Timers

object CartActor {
  sealed trait Command

  case class AddItem(item: Any)    extends Command
  case class RemoveItem(item: Any) extends Command
  case object ExpireCart           extends Command
  case object StartCheckout        extends Command
  case object CancelCheckout       extends Command
  case object CloseCheckout        extends Command

  sealed trait Event
  case class CheckoutStarted(checkoutRef: ActorRef) extends Event

  case object TickKey

  def props = Props(new CartActor())
}

class CartActor extends Actor with Timers {
  import CartActor._

  private val log                                    = Logging(context.system, this)
  val cartTimerDuration                              = 5 seconds
  implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

  private def scheduleTimer: Cancellable =
    context.system.scheduler.scheduleOnce(cartTimerDuration, self, ExpireCart)
  @Override
  def receive: Receive = empty

  def empty: Receive = LoggingReceive {
    case AddItem(item) =>
      val cart = Cart.empty
      context become nonEmpty(cart.addItem(item), scheduleTimer)
  }

  def nonEmpty(cart: Cart, timer: Cancellable): Receive = LoggingReceive {
    case AddItem(item) =>
      timer.cancel()
      context become nonEmpty(cart.addItem(item), scheduleTimer)

    case RemoveItem(item) =>
      timer.cancel()
      if (cart.contains(item)) {
        val newCart = cart.removeItem(item)
        if (newCart.size == 0) {
          context become empty
        } else {
          context become nonEmpty(newCart, scheduleTimer)
        }
      }

    case ExpireCart =>
      context become empty

    case StartCheckout =>
      timer.cancel()
      context become inCheckout(cart)
  }

  def inCheckout(cart: Cart): Receive = LoggingReceive {
    case CloseCheckout => context become empty
    case CancelCheckout => context become nonEmpty(cart, scheduleTimer)
  }
}
